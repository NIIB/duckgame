package org.DuckModel;

import java.awt.*;
import java.util.Random;

/**
 *  Waterlilies class allowing to set the board
 */
public class Waterlilies extends Grid {

    //Attribute X and Y position
    private int pos_x;
    private int pos_y;

    //Attribute Image waterlilies
    private Image rock_img;

    //Attribute number waterlilies max
    public static int itemsWaterlilies;

    // Constructor
    public Waterlilies() {
        setPos_x();
        setPos_y();
        while (map[getPos_x()][getPos_y()] != 0) {
            setPos_x();
            setPos_y();
        }
        setMap(getPos_x(), getPos_y(), getValue_map());
        map[getPos_x()][getPos_y()] = getValue_map();
        setNumberItemsWaterlilies();
    }

    //Method to set X position
    public void setPos_x() {
        this.pos_x = getRandomNumberInRangeToSetPos();;
    }

    //Method to set Y position
    public void setPos_y() {
        this.pos_y = getRandomNumberInRangeToSetPos();
    }

    //Method to get X position
    public int getPos_x() {
        return pos_x;
    }

    //Method to get Y position
    public int getPos_y() {
        return pos_y;
    }

    // Method to get value on the matrix
    private int getValue_map() {
        return 9;
    }

    // Method to get Rock image
    public Image getRock_img() {
        return rock_img;
    }

    // Method to set number waterlilies
    public void setNumberItemsWaterlilies(){
        itemsWaterlilies += 1;
    }

    public int getNumberItemsRock(){
        return itemsWaterlilies;
    }

    // Return random number between 1 and 15
    private static int getRandomNumberInRangeToSetPos() {
        Random r = new Random();
        return r.nextInt((15 - 1) + 1) + 1;
    }
}