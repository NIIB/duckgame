package org.DuckModel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Grid class allowing to set the board
 */

public class Grid extends JPanel {

    // Matrix representing the board
    public static int[][] map =
            {
                    {1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3},
                    {8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 9, 0, 9, 0, 0, 0, 0, 0, 9, 0, 9, 0, 0, 4},
                    {8, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 4},
                    {8, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 9, 0, 9, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 4},
                    {8, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 4},
                    {8, 0, 0, 9, 0, 9, 0, 0, 0, 0, 0, 9, 0, 9, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
                    {8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4},
                    {7, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
            };

    // Statement buffer to load image on the map
    BufferedImage left_background = null;
    BufferedImage right_background = null;
    BufferedImage top_background = null;
    BufferedImage bottom_background = null;
    BufferedImage middle_background = null;

    BufferedImage corner_top_left_background = null;
    BufferedImage corner_bottom_left_background = null;
    BufferedImage corner_top_right_background = null;
    BufferedImage corner_bottom_right_background = null;

    BufferedImage plant_background = null;

    BufferedImage dragonfly_left = null;
    BufferedImage dragonfly_right = null;
    BufferedImage dragonfly_up = null;
    BufferedImage dragonfly_down = null;

    BufferedImage DuckLeft = null;
    BufferedImage DuckRight = null;
    BufferedImage DuckUp = null;
    BufferedImage DuckDown = null;

    BufferedImage YoungDuckLeft = null;
    BufferedImage YoungDuckRight = null;
    BufferedImage YoungDuckUp = null;
    BufferedImage YoungDuckDown = null;


    /**
     * The function allowing to draw the grid with image and size map
     *
     * @param g graphic on windows
     */
    public void paint(Graphics g) {

        // We define  margin top and left for center the grid on the windows
        int margin_top_board = 75;
        int margin_left_board = 60;

        // With a double loop "for" on map matrix, we set our images
        for (int x_map = 0; x_map < map[1].length; x_map++) {

            for (int[] ints : map) {

                try {

                    middle_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\middle_background.png"));
                    left_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\left_background.png"));
                    right_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\right_background.png"));
                    top_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\top_background.png"));
                    bottom_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\bottom_background.png"));

                    corner_bottom_left_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\corner_bottom_left_background.png"));
                    corner_bottom_right_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\corner_bottom_right_background.png"));
                    corner_top_left_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\corner_top_left_background.png"));
                    corner_top_right_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\corner_top_right_background.png"));

                    dragonfly_left = ImageIO.read(new File("src\\main\\java\\Ressources\\Waterlilies\\Waterlilies_Left.png"));
                    dragonfly_right = ImageIO.read(new File("src\\main\\java\\Ressources\\Waterlilies\\Waterlilies_Right.png"));
                    dragonfly_up = ImageIO.read(new File("src\\main\\java\\Ressources\\Waterlilies\\Waterlilies_Up.png"));
                    dragonfly_down = ImageIO.read(new File("src\\main\\java\\Ressources\\Waterlilies\\Waterlilies_Down.png"));

                    DuckLeft = ImageIO.read(new File("src\\main\\java\\Ressources\\Duck\\Middle\\Duck_Left.png"));
                    DuckRight = ImageIO.read(new File("src\\main\\java\\Ressources\\Duck\\Middle\\Duck_Right.png"));
                    DuckUp = ImageIO.read(new File("src\\main\\java\\Ressources\\Duck\\Middle\\Duck_Up.png"));
                    DuckDown = ImageIO.read(new File("src\\main\\java\\Ressources\\Duck\\Middle\\Duck_Down.png"));

                    YoungDuckLeft = ImageIO.read(new File("src\\main\\java\\Ressources\\DuckYoung\\young_duck_left.png"));
                    YoungDuckRight = ImageIO.read(new File("src\\main\\java\\Ressources\\DuckYoung\\young_duck_right.png"));
                    YoungDuckUp = ImageIO.read(new File("src\\main\\java\\Ressources\\DuckYoung\\young_duck_top.png"));
                    YoungDuckDown = ImageIO.read(new File("src\\main\\java\\Ressources\\DuckYoung\\young_duck_down.png"));

                    plant_background = ImageIO.read(new File("src\\main\\java\\Ressources\\Background\\plant_items.png"));

                } catch (IOException e) {
                    System.out.print("Can't find an image");
                }

                // Optional, we can draw a grid on the background
                //g.drawRect(margin_top_board, margin_left_board, 25, 25);

                // Following a value on the matrix we load the corresponding image
                int value_map = ints[x_map];
                switch (value_map) {
                    case 0:
                        g.drawImage(middle_background, margin_top_board, margin_left_board, null);
                        break;
                    case 1:
                        g.drawImage(corner_top_left_background, margin_top_board, margin_left_board, null);
                        break;
                    case 2:
                        g.drawImage(top_background, margin_top_board, margin_left_board, null);
                        break;
                    case 3:
                        g.drawImage(corner_top_right_background, margin_top_board, margin_left_board, null);
                        break;
                    case 4:
                        g.drawImage(right_background, margin_top_board, margin_left_board, null);
                        break;
                    case 5:
                        g.drawImage(corner_bottom_right_background, margin_top_board, margin_left_board, null);
                        break;
                    case 6:
                        g.drawImage(bottom_background, margin_top_board, margin_left_board, null);
                        break;
                    case 7:
                        g.drawImage(corner_bottom_left_background, margin_top_board, margin_left_board, null);
                        break;
                    case 8:
                        g.drawImage(left_background, margin_top_board, margin_left_board, null);
                        break;
                    case 9:
                        g.drawImage(plant_background, margin_top_board, margin_left_board, null);
                        break;
                    case 11:
                        g.drawImage(DuckLeft, margin_top_board, margin_left_board, null);
                        break;
                    case 12:
                        g.drawImage(DuckDown, margin_top_board, margin_left_board, null);
                        break;
                    case 13:
                        g.drawImage(DuckRight, margin_top_board, margin_left_board, null);
                        break;
                    case 14:
                        g.drawImage(DuckUp, margin_top_board, margin_left_board, null);
                        break;
                    case 15:
                        g.drawImage(DuckDown, margin_top_board, margin_left_board, null);
                        break;
                    case 20:
                        g.drawImage(dragonfly_left, margin_top_board, margin_left_board, null);
                        break;
                    case 21:
                        g.drawImage(dragonfly_down, margin_top_board, margin_left_board, null);
                        break;
                    case 22:
                        g.drawImage(dragonfly_right, margin_top_board, margin_left_board, null);
                        break;
                    case 23:
                        g.drawImage(dragonfly_up, margin_top_board, margin_left_board, null);
                        break;
                    case 24:
                        g.drawImage(dragonfly_down, margin_top_board, margin_left_board, null);
                        break;
                    case 30:
                        g.drawImage(YoungDuckLeft, margin_top_board, margin_left_board, null);
                        break;
                    case 31:
                        g.drawImage(YoungDuckDown, margin_top_board, margin_left_board, null);
                        break;
                    case 32:
                        g.drawImage(YoungDuckRight, margin_top_board, margin_left_board, null);
                        break;
                    case 33:
                        g.drawImage(YoungDuckUp, margin_top_board, margin_left_board, null);
                        break;
                    case 34:
                        g.drawImage(YoungDuckDown, margin_top_board, margin_left_board, null);
                        break;
                }
                margin_left_board += 25;
            }
            margin_left_board = 60;
            margin_top_board += 25;
        }
    }

    /**
     * Setter items on the map
     */
    public void setMap(int setPosX, int setPosY, int getValue) {
        this.map[setPosX][setPosY] = getValue;
    }

    /**
     * Getter items on the map
     *
     * @return map
     */
    public int[][] getMapItem() {
        return map;
    }

}


