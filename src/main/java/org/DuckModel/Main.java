package org.DuckModel;

import javax.swing.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        // Statement attribute starting moving items
        boolean startUpMovingItems = true;

        // Statement an new JFRAME
        JFrame frame = new JFrame();

        //Statement generic list to store object 'Duck' and 'Waterlilies'
        ArrayList<Duck> arrayListItemsDuck = new ArrayList<Duck>();
        ArrayList<DragonFly> arrayListItemsDragonFly = new ArrayList<DragonFly>();

        // Set various parameters windows
        frame.setSize(600, 600);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setTitle("Duck Game");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Statement new object which is extends to a JPANEL
        Grid board = new Grid();

        // Add JPANEL on JFRAME
        frame.getContentPane().add(board);

        //We set and moves items on the board
        while (startUpMovingItems) {

            // Generation object duck
            while (arrayListItemsDuck.size() <= 2) {
                // Add on board, array object duck and repaint windows
                Duck init_items_duck = new Duck();
                board.repaint();
                arrayListItemsDuck.add(init_items_duck);
            }

            // Generation object water lilies
            while (arrayListItemsDragonFly.size() <= 5) {
                // Add on board, array object water lilies and repaint windows
                DragonFly init_items_DragonFly = new DragonFly();
                board.add(init_items_DragonFly);
                board.repaint();
                arrayListItemsDragonFly.add(init_items_DragonFly);
            }

            // For each object previously instantiate we create one thread associate
            for (int i = 0; arrayListItemsDuck.size() > i; i++) {
                Duck objItems = arrayListItemsDuck.get(i);
                if (objItems.getHealth() <= 0) {
                    //We delete object if life is equal = 0 and repaint board
                    Grid.map[objItems.getPos_x()][objItems.getPos_y()] = 0;
                    board.repaint();
                    //We delete object store in array list object
                    arrayListItemsDuck.remove(i);

                } else {
                    // We generate random number to set X random move
                        // We start thread and repaint the board
                        Thread thread = new Thread(objItems);
                        thread.start();
                        board.repaint();
                        try {
                            // We wait for the thread to end before starting the thread of the following object
                            thread.join();
                            board.repaint();
                        } catch (Exception ex) {
                            System.out.println("Exception has " + "been caught" + ex);
                        }
                    }

                // For each object previously instantiate we create one thread associate
                arrayListItemsDragonFly.stream().map(Thread::new).forEach(thread -> {
                    // We start thread and repaint the board
                    thread.start();
                    board.repaint();
                    try {
                        // We wait for the thread to end before starting the thread of the following object
                        thread.join();
                        board.repaint();
                    } catch (Exception ex) {
                        System.out.println("Exception has " + "been caught" + ex);
                    }
                });
            }
        }
    }
}



