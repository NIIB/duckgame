package org.DuckModel;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Duck class allowing to set the board
 */
public class Duck extends Grid implements Runnable {

    // Statement attributes max number duck
    public static int itemsDuck;

    // Statement attributes X and Y for position on the board
    private int pos_x;
    private int pos_y;

    // Statement attribute time to live
    public int health = 10;

    // Statement upgrade ou downgrade
    private int level = 0;

    boolean moving = true;

    // Statement value map for board
    private int Value_map;

    // Statement possible movement around them
    private final int[][] position = {
            {-1, 0}, // Duck Up    [0]
            {1, 0},  // Duck down  [1]
            {0, 1},  // Duck right [2]
            {0, -1}, // Duck left  [3]
    };

    // Constructor to set position
    public Duck() {
        setPos_x();
        setPos_y();
        while (map[getPos_x()][getPos_y()] != 0) {
            setPos_x();
            setPos_y();
        }
        setMap(getPos_x(), getPos_y(), getValue_map());
        map[getPos_x()][getPos_y()] = getValue_map();
        setNumberDuck();
    }

    //Method to check or eat water_lilies
    private void checkOrEatWaterlilies() {
        //First, we check all around the duck
        for (int i = 0; i <= 3; i++) {
            try {
                int[] checkArround = position[i];
                int checkAround_X = checkArround[0];
                int checkAround_Y = checkArround[1];

                // If waterlilies is around, he is eaten by duck
                if (map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] == 9) {

                    int xMapItemBeforeUpdate = getPos_x();
                    int yMapItemBeforeUpdate = getPos_y();

                    switch (i) {
                        case 0:
                            if (level >= 6) {
                                this.Value_map = 33;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 33;
                            } else {
                                this.Value_map = 14;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 14;
                            }
                            break;
                        case 1:
                            if (level >= 6) {
                                this.Value_map = 31;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 31;
                            } else {
                                this.Value_map = 12;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 12;
                            }
                            break;
                        case 2:
                            if (level >= 6) {
                                this.Value_map = 32;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 32;
                            } else {
                                this.Value_map = 13;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 13;
                            }
                            break;
                        case 3:
                            if (level >= 6) {
                                this.Value_map = 30;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 30;
                            } else {
                                this.Value_map = 11;
                                map[(getPos_x() + checkAround_X)][(getPos_y() + checkAround_Y)] = 11;
                            }
                            break;
                    }

                    // Reset life
                    health = 10;

                    //Instantiate new waterlilies
                    Waterlilies newWaterlilies = new Waterlilies();

                    map[xMapItemBeforeUpdate][yMapItemBeforeUpdate] = 0;
                    this.pos_x = (getPos_x() + checkAround_X);
                    this.pos_y = (getPos_y() + checkAround_Y);
                    this.level += 2;
                    repaint();
                    moving = false;
                    break;
                }

            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("Out of board");
            }
        }
    }

    // Method to move items on bard
    private void moving() {

        moving = true;

        // Get initials items position
        int xMapItemBeforeUpdate = getPos_x();
        int yMapItemBeforeUpdate = getPos_y();

        checkOrEatWaterlilies();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        repaint();

        if (moving) {
            // Apply random move
            this.isValidMove();

            // While case board is not available we recall 'isValidMove()' with the current object
            while (map[getPos_x()][getPos_y()] != 0) {
                removeNewPos_X(xMapItemBeforeUpdate);
                removeNewPos_Y(yMapItemBeforeUpdate);
                map[getPos_x()][getPos_y()] = getValue_map();
                isValidMove();
            }

            // if case board is available we update the map
            map[xMapItemBeforeUpdate][yMapItemBeforeUpdate] = 0;
            map[getPos_x()][getPos_y()] = getValue_map();
        }
        // We decrement attribute health for each movement;
        setHealth(1);

    }

    // Method to set random Position X
    private void setPos_x() {
        this.pos_x = getRandomNumberInRangeToSetPos();
    }

    // Method to set random Position Y
    private void setPos_y() {
        this.pos_y = getRandomNumberInRangeToSetPos();
    }

    // Return random number between 1 and 15
    private static int getRandomNumberInRangeToSetPos() {
        Random r = new Random();
        return r.nextInt((15 - 1) + 1) + 1;
    }

    // Method to get Position Y
    public int getPos_y() {
        return pos_y;
    }

    // Method to get Position X
    public int getPos_x() {
        return pos_x;
    }

    // Set value on the duck move
    public int setValue_map(int randomPosSelected) {
        switch (randomPosSelected) {
            case 0:
                if (level >= 6) {
                    this.Value_map = 33;
                } else {
                    this.Value_map = 14;
                }
                break;
            case 1:
                if (level >= 6) {
                    this.Value_map = 31;
                } else {
                    this.Value_map = 12;
                }
                break;
            case 2:
                if (level >= 6) {
                    this.Value_map = 32;
                } else {
                    this.Value_map = 13;
                }
                break;
            case 3:
                if (level >= 6) {
                    this.Value_map = 30;
                } else {
                    this.Value_map = 11;
                }
                break;
            default:
                if (level >= 6) {
                    this.Value_map = 34;
                } else {
                    this.Value_map = 15;
                }
                break;
        }
        return Value_map;
    }

    // Method to get value on the matrix
    private int getValue_map() {
        return Value_map;
    }

    // Method to move items on matrix
    private void isValidMove() {
        try {
            TimeUnit.MILLISECONDS.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Get random number between 0 to 3
        int getRandomMoving = getRandomNumberInRangeToSetMove();

        // Select random Position "ex: -1 ,0"
        int[] setRandomMoving = position[getRandomMoving];
        int setRandomMovingX = setRandomMoving[0];
        int setRandomMovingY = setRandomMoving[1];

        // Following random position selected with set X and Y position
        this.pos_x += setRandomMovingX;
        this.pos_y += setRandomMovingY;
        this.Value_map = setValue_map(getRandomMoving);
    }

    // Method to get random random possible movement
    public static int getRandomNumberInRangeToSetMove() {
        Random r = new Random();
        return r.nextInt((3) + 1);
    }

    // Method to remove X position if case if not available
    private void removeNewPos_X(int removePos_x) {
        this.pos_x = removePos_x;
    }

    // Method to remove Y position if case if not available
    private void removeNewPos_Y(int removePos_y) {
        this.pos_y = removePos_y;
    }

    /**
     * Set number Duck on the map
     */
    private void setNumberDuck() {
        itemsDuck += 1;
    }

    /**
     * Get number Duck
     *
     * @return int items Duck
     */
    public int getNumberDuck() {
        return itemsDuck;
    }

    /**
     * Method to get duck health
     * <p>
     * return int health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Method to set duck health
     * param int health = value decrement
     */
    public void setHealth(int health) {
        this.health -= health;
    }

    /**
     * Method runnable to move duck
     */
    @Override
    public void run() {
        repaint();
        moving();
        repaint();
    }
}
