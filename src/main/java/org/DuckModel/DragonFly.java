package org.DuckModel;

import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Waterlilies class allowing to set these items
 */
public class DragonFly extends Grid implements Runnable {

    // Statement attributes X and Y for position on the board
    private int pos_x;
    private int pos_y;

    // Statement attributes max number DragonFly
    public static int itemsDragonFly;

    // Statement value map for board
    public int Value_map;

    // Statement image items
    public BufferedImage ImgItems;

    // Statement possible movement around them
    private final int[][] position = {
            {-1, 0},  // Waterlilies Up     [0]
            {1, 0},   // Waterlilies down   [1]
            {0, 1},   // Waterlilies right  [2]
            {0, -1},  // Waterlilies left   [3]
            {0, 0},   // Water lilies same place
    };

    // Constructor to set position
    public DragonFly() {
        setPos_x();
        setPos_y();
        while (map[getPos_x()][getPos_y()] != 0) {
            setPos_x();
            setPos_y();
        }
        setMap(getPos_x(), getPos_y(), getValue_map());
        map[getPos_x()][getPos_y()] = getValue_map();
        setNumberDragonFly();
    }

    // Method to move items on bard
    private void moving() {
        // Get initials items position
        int xMapItemBeforeUpdate = getPos_x();
        int yMapItemBeforeUpdate = getPos_y();

        // Apply random move
        this.isValidMove();

        // While case board is not available we recall 'isValidMove()' with the current object
        while (map[getPos_x()][getPos_y()] != 0) {
            removeNewPos_X(xMapItemBeforeUpdate);
            removeNewPos_Y(yMapItemBeforeUpdate);
            map[getPos_x()][getPos_y()] = getValue_map();
            isValidMove();
        }
        // if case board is available we update the map
        map[xMapItemBeforeUpdate][yMapItemBeforeUpdate] = 0;
        map[getPos_x()][getPos_y()] = getValue_map();

    }

    // Method to set random Position X
    public void setPos_x() {
        this.pos_x = getRandomNumberInRangeToSetPos();
    }

    // Method to set random Position Y
    public void setPos_y() {
        this.pos_y = getRandomNumberInRangeToSetPos();
    }

    // Return random number between 1 and 15
    private static int getRandomNumberInRangeToSetPos() {
        Random r = new Random();
        return r.nextInt((15 - 1) + 1) + 1;
    }

    // Method to get Position Y
    public int getPos_y() {
        return pos_y;
    }

    // Method to get Position X
    public int getPos_x() {
        return pos_x;
    }

    // Set value on the DragonFly move
    private int setValue_map(int randomPosSelected) {
        switch (randomPosSelected) {
            case 0:
                this.Value_map = 23;
                break;
            case 1:
                this.Value_map = 21;
                break;
            case 2:
                this.Value_map = 22;
                break;
            case 3:
                this.Value_map = 20;
                break;
            default:
                this.Value_map = 24;
        }
        return Value_map;
    }

    // Method to get value on the matrix
    private int getValue_map() {
        return Value_map;
    }

    // Method to move items on matrix
    public void isValidMove() {
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Get random number between 0 to 4
        int getRandomMoving = getRandomNumberInRangeToSetMove();

        // Select random Position "ex: -1 ,0"
        int[] setRandomMoving = position[getRandomMoving];
        int setRandomMovingX = setRandomMoving[0];
        int setRandomMovingY = setRandomMoving[1];

        // Following random position selected with set X and Y position
        this.pos_x += setRandomMovingX;
        this.pos_y += setRandomMovingY;
        this.Value_map = setValue_map(getRandomMoving);
    }

    // Method to remove X position if case if not available
    public void removeNewPos_X(int removePos_x) {
        this.pos_x = removePos_x;
    }

    // Method to remove Y position if case if not available
    public void removeNewPos_Y(int removePos_y) {
        this.pos_y = removePos_y;
    }

    // Method to get random random possible movement
    private static int getRandomNumberInRangeToSetMove() {
        Random r = new Random();
        return r.nextInt((4) + 1);
    }

    // Method to get image according to the movement
    public BufferedImage getImgItems() {
        return ImgItems;
    }

    /**
     * Set number Dragonfly on the map
     */
    private void setNumberDragonFly() {
        itemsDragonFly += 1;
    }

    /**
     * Get number Dragonfly
     *
     * @return int items Dragonfly
     */
    public int getNumberDragonFly() {
        return itemsDragonFly;
    }

    /**
     * Method runnable to dragonfly
     */
    @Override
    public void run() {
        moving();
    }

}

